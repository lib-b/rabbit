package com.test.controller;

import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.test.service.Send;

@Controller
public class SendMessage {
	@Autowired
	private Send send;

	@RequestMapping("/sendMsg")
	@ResponseBody
	public Map<String, Object> sendMsg(String message) {
		for (int i = 0; i < 100; i++) {
			send.sendMsg(message + i);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("status", true);
		return map;
	}
}
