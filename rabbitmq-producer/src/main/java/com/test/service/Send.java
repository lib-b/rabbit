package com.test.service;

import java.util.UUID;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.support.CorrelationData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.test.config.AqConfig;

@Component
public class Send {
	@Autowired
	private RabbitTemplate rabbitTemplate;

	public void sendMsg(String content) {
		CorrelationData correlationData = new CorrelationData(UUID.randomUUID().toString());
		rabbitTemplate.convertAndSend(AqConfig.EXCHANGE,AqConfig.ROUTINGKEY,content,correlationData);
	}

}
